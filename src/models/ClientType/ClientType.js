import BaseModel from "../BaseModel";

export default class ClientType extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null,
    };
  }

  routes() {
    return {
      fetch: "/client-type/{id}",
      save: "/client-type",
      delete: "client-type/{id}",
      update: "client-type/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
