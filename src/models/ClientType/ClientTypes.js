import BaseCollection from "../BaseCollection";
import ClientType from "./ClientType";

export default class ClientTypes extends BaseCollection {
  model() {
    return ClientType;
  }
  defaults() {
    return {
      orderBy: "name"
    };
  }
  routes() {
    return {
      fetch: "client-type"
    };
  }
}
