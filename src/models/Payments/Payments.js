import BaseCollection from "../BaseCollection";
import Payment from "./Payment";

export default class Payments extends BaseCollection {
  model() {
    return Payment;
  }
  defaults() {
    return {
      orderBy: "id",
      saleId: null
    };
  }
  routes() {
    return {
      fetch: "sale/{saleId}/payments"
    };
  }
}
