import BaseModel from "../BaseModel";

export default class Payment extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      sale_id: null,
      payment_method_id: null,
      payment_type_id: null,
      total_paid: 0,
      date: new Date().toISOString().substr(0, 10),
    };
  }

  routes() {
    return {
      fetch: "/payment/{id}",
      save: "/payment",
      delete: "payment/{id}",
      update: "payment/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
