import { Model } from "vue-mc";


export default class BaseModel extends Model {
  getRequest(config, requireAuth=true) {
    config.baseURL = process.env.VUE_APP_API_BASE_URL;
    //config.baseURL = config.baseURL = process.env.BASE_URL;
    var token = localStorage.getItem('token');
    if(requireAuth && token) {
      config.headers = { 'Authorization': 'Bearer ' + token,};
    }
    return super.getRequest(config);
  }

  getUrl(params) {
    return super.getURL(params);
  }

  fetchForShop() {
    let method = this.getOption("methods.shop");
    let route = this.getRoute("shop");
    let params = this.getRouteParameters();
    let url = this.getUrl(route);

    this.getRequest({ method, url })
      .send()
      .then(response => {
        let attributes = response.getData();
        this.assign(attributes.data);
      })
      .catch(error => {
        console.log(error);
      });
  }


}
