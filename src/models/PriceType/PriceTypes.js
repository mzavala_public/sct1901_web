import BaseCollection from "../BaseCollection";
import PriceType from "./PriceType";

export default class PriceTypes extends BaseCollection {
  model() {
    return PriceType;
  }
  defaults() {
    return {
      orderBy: "name"
    };
  }
  routes() {
    return {
      fetch: "price-type"
    };
  }
}
