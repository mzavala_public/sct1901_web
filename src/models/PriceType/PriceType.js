import BaseModel from "../BaseModel";

export default class PriceType extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null,
      min_qty: null
    };
  }

  routes() {
    return {
      fetch: "/price-type/{id}",
      save: "/price-type",
      delete: "price-type/{id}",
      update: "price-type/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
