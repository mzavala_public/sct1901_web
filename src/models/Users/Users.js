import BaseCollection from "../BaseCollection";
import User from "./User";

export default class Users extends BaseCollection {
  model() {
    return User;
  }

  defaults() {
    return {
      orderBy: "name"
    };
  }

  routes() {
    return {
      fetch: "users"
    };
  }
}
