import BaseModel from "../BaseModel";

export default class User extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null,
      username: null,
      password: null,
      email: null,
    };
  }


  routes() {
    return {
      fetch: "/users/{id}",
      save: "/users",
      delete: "/users/{id}",
      update: "users/{id}",
      auth: "/auth/login",
      user: "/user"
    };
  }

  options() {
    return {
      methods: {
        update: "PUT",
        auth: "POST",
        user: "USER"
      }
    };
  }

  getUrl(params) {
    return super.getURL(params);
  }

  auth() {
    let data = { identity:  this.username, password: this.password};
    return this.customRequest("methods.auth","auth",data,false);
  }

  getCurrentUser() {
    this.customRequest("methods.user","user",null,true)
    .then(
      response => {
        let attributes = response.getData();
        this.assign(attributes.data);
      }
    );
  }

  customRequest(methodName, routeName, data, requireAuth ) {
    let method = this.getOption(methodName);
    let route = this.getRoute(routeName);
    let params = this.getRouteParameters();
    let url = this.getUrl(route);

    return this.getRequest({ method, url, data }, requireAuth).send();
  }

 
}
