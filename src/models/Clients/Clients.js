import BaseCollection from "../BaseCollection";
import Client from "./Client";

export default class Clients extends BaseCollection {
  model() {
    return Client;
  }
  defaults() {
    return {
      orderBy: "name"
    };
  }
  routes() {
    return {
      fetch: "client",
      search: "search/client"
    };
  }

  options() {
    return {
      methods: {
        search: "POST"
      }
    };
  }

  
}
