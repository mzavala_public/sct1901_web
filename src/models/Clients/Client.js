import BaseModel from "../BaseModel";

export default class Client extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null,
      client_type_id: null,
      total_amount: 0,
      total_paid: 0,
      balance: 0
    };
  }

  routes() {
    return {
      fetch: "/client/{id}",
      save: "/client",
      delete: "client/{id}",
      update: "client/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
