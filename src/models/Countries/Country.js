import BaseModel from "../BaseModel";
import {
  boolean,
  equal,
  integer,
  min,
  required,
  string
} from "vue-mc/validation";
export default class Country extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null
    };
  }

  validation() {
    return {
      id: integer.and(min(1)).or(equal(null)),
      name: string.and(required)
    };
  }

  routes() {
    return {
      fetch: "/countries/{id}",
      save: "/countries",
      update: "/countries/{id}",
      delete: "/countries/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
