import BaseCollection from "../BaseCollection";
import PaymentMethod from "./PaymentMethod";

export default class PaymentMethods extends BaseCollection {
  model() {
    return PaymentMethod;
  }
  defaults() {
    return {
      orderBy: "name"
    };
  }
  routes() {
    return {
      fetch: "payment-method"
    };
  }
}
