import BaseModel from "../BaseModel";

export default class PaymentMethod extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null
    };
  }

  routes() {
    return {
      fetch: "/payment-method/{id}",
      save: "payment-method",
      delete: "payment-method/{id}",
      update: "payment-method/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
