import BaseCollection from "../BaseCollection";
import Seller from "./Seller";

export default class Sellers extends BaseCollection {
  model() {
    return Seller;
  }
  defaults() {
    return {
      orderBy: "name"
    };
  }
  routes() {
    return {
      fetch: "sellers",
      search: "search/sellers"
    };
  }

  options() {
    return {
      methods: {
        search: "POST"
      }
    };
  }
}
