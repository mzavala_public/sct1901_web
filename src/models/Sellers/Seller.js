import BaseModel from "../BaseModel";

export default class Seller extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null,
      commission: null
    };
  }

  routes() {
    return {
      fetch: "/sellers/{id}",
      save: "/sellers",
      delete: "sellers/{id}",
      update: "sellers/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
