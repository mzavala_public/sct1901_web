import BaseCollection from "../BaseCollection";
import PaymentType from "./PaymentType";

export default class PaymentTypes extends BaseCollection {
  model() {
    return PaymentType;
  }
  defaults() {
    return {
      orderBy: "name"
    };
  }
  routes() {
    return {
      fetch: "payment-type"
    };
  }
}
