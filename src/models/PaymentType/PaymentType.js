import BaseModel from "../BaseModel";

export default class PaymentType extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null,
    };
  }

  routes() {
    return {
      fetch: "/payment-type/{id}",
      save: "/payment-type",
      delete: "payment-type/{id}",
      update: "payment-type/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
