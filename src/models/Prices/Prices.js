import BaseCollection from "../BaseCollection";
import Price from "./Price";

export default class Prices extends BaseCollection {

  model() {
    return Price;
  }
  defaults() {
    return {
      orderBy: "name",
      productId: null
    };
  }
  routes() {
    return {
      fetch: "products/{productId}/prices"
    };
  }
}
