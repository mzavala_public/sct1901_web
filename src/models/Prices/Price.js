import BaseModel from "../BaseModel";

export default class Price extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      unit_price: 0,
      product_id: null,
      price_type_id: null,
      description: ''
    };
  }

  routes() {
    return {
      fetch: "/price/{id}",
      save: "/price",
      delete: "price/{id}",
      update: "price/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
