import BaseModel from "../BaseModel";
import Prices from "../Prices/Prices";

import {
  boolean,
  equal,
  integer,
  min,
  required,
  string
} from "vue-mc/validation";
export default class Product extends BaseModel {
  constructor(props) {
    super(props);
  }
  defaults() {
    return {
      id: null,
      name: null,
      description: null,
      contains: null,
      barcode: null,
      formula: null,
      quantum_satis: null,
      category_id: null,
      via_id: null,
      country_id: null,
      stock: 0
    };
  }
  validation() {
    return {
      id: integer.and(min(1)).or(equal(null)),
      name: string.and(required),
      contains: string.and(required),
      barcode: string.and(required),
      formula: string.and(required),
      quantum_satis: string.and(required)
    };
  }

  routes() {
    return {
      fetch: "/products/{id}",
      save: "/products",
      delete: "products/{id}",
      update: "products/{id}",
    };
  }
  options() {
    return {
      methods: {
        update: "PUT",
        getPrices: "GET"
      }
    };
  }

}
