import { Collection } from "vue-mc";

export default class BaseCollection extends Collection {
  getRequest(config, requireAuth=true) {
    config.baseURL = process.env.VUE_APP_API_BASE_URL;
    config.headers = { "Access-Control-Allow-Origin": "*" };
    var token = localStorage.getItem('token');
    if(requireAuth && token) {
      config.headers = { 'Authorization': 'Bearer ' + token,};
    }
    return super.getRequest(config);
  }

  getUrl(params) {
    return super.getURL(params);
  }

 
  search(param) {
    let method = this.getOption("methods.search");
    let route = this.getRoute("search");
    let params = this.getRouteParameters();
    let url = this.getUrl(route);
    let data = { name: param.name };

    this.getRequest({ method, url, data })
      .send()
      .then(response => {
        let models = this.getModelsFromResponse(response);
        this.replace(models);
      })
      .catch(error => {
        console.log(error);
      });
  }

  fetchForShop() {
    let method = this.getOption("methods.shop");
    let route = this.getRoute("shop");
    let params = this.getRouteParameters();
    let url = this.getUrl(route);

    this.getRequest({ method, url })
      .send()
      .then(response => {
        let models = this.getModelsFromResponse(response);
        this.replace(models);
      })
      .catch(error => {
        console.log(error);
      });
  }
}
