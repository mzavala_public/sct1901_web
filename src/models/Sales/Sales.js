import BaseCollection from "../BaseCollection";
import Sale from "./Sale";

export default class Sales extends BaseCollection {
  model() {
    return Sale;
  }
  defaults() {
    return {
      orderBy: "created_at"
    };
  }
  routes() {
    return {
      fetch: "sale"
    };
  }
}
