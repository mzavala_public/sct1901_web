/* eslint-disable prettier/prettier */
import BaseModel from "../BaseModel";

import {
  boolean,
  equal,
  integer,
  min,
  required,
  string
} from "vue-mc/validation";

export default class Seller extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      total_amount: 0,
      total_paid: 0,
      balance: 0,
      seller_id: null,
      sale_method_id: null,
      client_id: null,
      details: [],
      concepts: [],
      date: new Date().toISOString().substr(0, 10)
    };
  }


  routes() {
    return {
      fetch: "/sale/{id}",
      save: "/sale",
      delete: "sale/{id}",
      update: "sale/{id}"
    };
  }
  options() {
    return {
      validateOnChange: true,
      methods: {
        update: "PUT"
      }
    };
  }

  static  validate(sale) {
    let valid = 
    !!sale.client_id && 
    !!sale.sale_method_id;
    return valid;
  }
}
