import BaseModel from "../BaseModel";

export default class SaleMethod extends BaseModel {
  constructor(props) {
    super(props);
  }

  defaults() {
    return {
      id: null,
      name: null
    };
  }

  routes() {
    return {
      fetch: "/sales-method/{id}",
      save: "/sales-method",
      delete: "sales-method/{id}",
      update: "sales-method/{id}"
    };
  }
  options() {
    return {
      methods: {
        update: "PUT"
      }
    };
  }
}
