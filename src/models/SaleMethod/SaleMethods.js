import BaseCollection from "../BaseCollection";
import SaleMethod from "./SaleMethod";

export default class SaleMethods extends BaseCollection {
  model() {
    return SaleMethod;
  }
  defaults() {
    return {
      orderBy: "name"
    };
  }
  routes() {
    return {
      fetch: "sales-method"
    };
  }
}
