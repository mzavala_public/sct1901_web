import Vue from "vue";
import store from "./store";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Dashboard from "./views/admin/Dashboard.vue";
import ProductsMain from "./views/admin/Products/ProductsMain.vue";
import CategoriesMain from "./views/admin/Categories/CategoriesMain.vue";
import ViasMain from "./views/admin/Vias/ViasMain.vue";
import ProductDetails from "./views/shop/ProductDetails.vue"
import CountriesMain from "./views/admin/Countries/CountriesMain.vue";
import Inicio from "./views/Inicio.vue"
import Products from "./views/shop/Products.vue";
import SellersMain from "./views/admin/Sellers/SellersMain.vue";
import SaleMethodsMain from "./views/admin/SaleMethods/SaleMethodsMain.vue";
import PriceTypeMain from "./views/admin/PriceType/PriceTypeMain.vue"
import PaymentMethodsMain from "./views/admin/PaymentMethods/PaymentMethodsMain.vue"
import PaymentTypeMain from "./views/admin/PaymentType/PaymentTypeMain.vue"
import ConfigProducts from "./views/admin/Products/ConfigProducts.vue"
import ClientTypeMain from "./views/admin/ClientTypes/ClientTypeMain.vue"
import ClientsMain from "./views/admin/Clients/ClientsMain.vue"
import SalesMain from "./views/admin/Sales/SalesMain.vue"
import CreateUpdateSale from "./views/admin/Sales/CreateUpdateSale.vue"
import ShowSaleDetail from "./views/admin/Sales/ShowSaleDetail.vue"
import Login from "./views/admin/Users/Login.vue"

Vue.use(Router);
let router =  new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: { 
        guest: true
      },
      children: [
        {
          path:"",
          name:"inicio",
          component: Inicio
        },
        {
          path:"products/:id/detail/",
          name:"product_detail",
          component: ProductDetails,
          props : {
            name: 'click'
          }
        },
        {
          path:"products",
          name:"products_shop",
          component: Products
        }
      ]
    },
    {
      path: "/login",
      name: "auth",
      component: Login
    },
    {
      path: "/admin",
      name: "admin",
      component: Dashboard,
      meta: { 
        requiresAuth: true
      },
      children: [
        {
          path: "products",
          name: "products",
          component: ProductsMain,
        },
        {
          path: "products/:id/config",
          name: "config-product",
          component: ConfigProducts
        },
        {
          path: "categories",
          name: "categories",
          component: CategoriesMain
        },
        {
          path: "vias",
          name: "vias",
          component: ViasMain
        },
        {
          path: "countries",
          name: "countries",
          component: CountriesMain,
        },
        {
          path: "sellers",
          name: "sellers",
          component: SellersMain,
        },
        {
          path: "sale-methods",
          name: "sale-methods",
          component: SaleMethodsMain,
        },
        {
          path: "price-types",
          name: "price-types",
          component: PriceTypeMain,
        },
        {
          path: "payment-methods",
          name: "payment-methods",
          component: PaymentMethodsMain,
        },
        {
          path: "payment-types",
          name: "payment-types",
          component: PaymentTypeMain,
        },
        {
          path: "clients",
          name: "clients",
          component: ClientsMain,
        },
        {
          path: "client-types",
          name: "client-types",
          component: ClientTypeMain,
        },
        {
          path: "sales",
          name: "sales",
          component: SalesMain,
        },
        {
          path: "sales/new",
          name: "sales-new",
          component: CreateUpdateSale,
          props: true
        },
        {
          path: "sales/edit",
          name: "sales-edit",
          component: CreateUpdateSale,
          props: true
        },
        {
          path: "sales/:id/detail",
          name: "sales-detail",
          component: ShowSaleDetail
        }
      ]
    },
    {
      path: "/about",
      name: "about",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    }
  ]
});
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login') 
  }else {
    next() 
  }
})
export default router
