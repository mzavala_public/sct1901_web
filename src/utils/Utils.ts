import moment from "moment";

export default class Utils {
static formatCurrency(val:number): string { 
    return val ? Utils.currency(val,"$") : Utils.currency(0,"$");
}

private static currency(value:number, currency:string): string {
    var val:string = (value/1).toFixed(2).replace(',', '.');
    return currency+ " " + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

static formatDate(val:string): string {
return val ? moment(String(val)).format('DD/MM/YYYY') : 'N/A';
}
}
