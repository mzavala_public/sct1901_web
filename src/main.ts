import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "./registerServiceWorker";
import Axios from "axios";
import Utils from "@/utils/Utils"


Vue.filter("date", function(val:string){
  return Utils.formatDate(val);
});

Vue.filter("money", function(val:number){
  return Utils.formatCurrency(val);
});

Vue.config.productionTip = false;
Vue.prototype.$http = Axios;
Vue.use(Vuetify, {
  theme: {
    primary: "#1c8bd0",
    secondary: "#0852b5",
    accent: "#009BDE",
    error: "#FF5252",
    info: "#2196F3",
    success: "#00B23D",
    warning: "#FF7F50",
    save: "#292aa8"
  },
  breakpoint: {
    thresholds: {
      xs: 360
    },
    scrollbarWidth: 10
  }
});
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
