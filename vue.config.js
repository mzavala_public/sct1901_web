module.exports = {
  publicPath: "/app",
  configureWebpack: {
    devtool: "source-map"
  },
  devServer: {
    https: process.env.NODE_ENV === 'production'
  }
};
